/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

define('source-editor/source-editor-cm-search-adapter', [
], function (
) {
    // Implementation for CodeMirror 2. Replace this to use an alternate editor
    return function (editor) {
        var state = {};
        var e = editor;
        var cursor;

        function clearState() {
            state.from = state.to = e.getCursor();
            state.query = '';
        }

        function saveState() {
            if (cursor) {
                state.from = (cursor && cursor.from()) || state.from;
                state.to = (cursor && cursor.to()) || state.to;
            }
        }

        function restartFind() {
            cursor = e.getSearchCursor(state.query, { line: 0, ch: 0 }, !state.matchCase);
            return find(false);
        }

        function find(options) {
            var match;
            if (cursor) {
                match = cursor.find(options.backward);
                if (!match && options.wrap) {
                    // try from start / end
                    cursor = e.getSearchCursor(state.query, options.backward ? { line: e.lineCount() - 1 } : { line: 0, ch: 0 }, !state.matchCase);
                    match = cursor.find(options.backward);
                }
                if (match) {
                    saveState();
                    e.setSelection(state.from, state.to);
                } else {
                    e.setCursor(state.from);
                }
            }
            return match;
        }

        function replace(text, wrap) {
            if (cursor) {
                var start = cursor.from();

                cursor.replace(text);
                return find({ backward: false, wrap: wrap });
            }
        }

        function replaceAll(text) {
            var count = 0;
            if (cursor) {
                restartFind();
                while (replace(text, false)) {
                    count++;
                }
                editor.focus();
            }
            return count;
        }

        clearState();

        var adapter = {
            findPrev: function () {
                return e.operation(function () {
                    return find({ backward: true, wrap: true });
                });
            },

            findNext: function () {
                return e.operation(function () {
                    return find({ backward: false, wrap: true });
                });
            },

            replace: function (text) {
                return e.operation(function () {
                    return replace(text, true);
                });
            },

            replaceAll: function (text) {
                return e.operation(function () {
                    return replaceAll(text);
                });
            },

            startFind: function (text, isRegex, matchCase) {
                return e.operation(function () {
                    var flags = matchCase ? 'g' : 'gi';
                    state.query = isRegex ? new RegExp(text, flags) : text;
                    state.matchCase = matchCase;
                    cursor = e.getSearchCursor(state.query, state.from, !state.matchCase);
                    return find({ backforward: false, wrap: true });
                });
            },

            clearSearch: function () {
                e.operation(function () {
                    clearState();
                    e.setSelection(state.from, state.to);
                    cursor = undefined;
                });
            }
        };

        return adapter;
    };
});


// var $d = $(document);

// $d.bind('source-editor-init', function() {
